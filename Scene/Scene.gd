extends Node2D

signal reward()
signal new_point(list)

var rng = RandomNumberGenerator.new()
var checkpoints = 5
export(int, 500, 1000) var distance = 500
export(int, 45) var max_alpha = 45
var list = Array()
var checkpoint = preload("res://Checkpoint/CheckPoint.tscn")

func _ready():
	rng.randomize()
	var prev_prev = Vector2(0, distance)
	var prev = Vector2()
	for _i in range(checkpoints):
		_generate(prev_prev, prev)
		prev_prev = prev
		prev = list[len(list) - 1].get_position()
	_emit_positions()
	
func _generate(prev_prev, prev):
	var curr = prev - prev_prev
	var alpha = rng.randi_range(-max_alpha, max_alpha)
	var rotated = curr.rotated(deg2rad(alpha))
	
	var ch = checkpoint.instance()
	ch.connect("body_entered_area", self, "_player_have_reached_checkpoint")
	ch.position = prev + rotated
	list.append(ch)
	call_deferred("add_child", ch)

func _emit_positions():
	var positions_list = Array()
	for ch in list:
		positions_list.append(ch.get_position())
	emit_signal("new_point", positions_list)

func _player_have_reached_checkpoint(area):
	if (list[0] != area):
		return
	var prev_prev = list[len(list) - 2].get_position()
	var prev = list[len(list) - 1].get_position()
	_generate(prev_prev, prev)
	list[0].queue_free()
	list.pop_front()
	emit_signal("reward")
	_emit_positions()
