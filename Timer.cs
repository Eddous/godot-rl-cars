using System;

public class Timer
{
    float time;
    float max_time;
    public Timer(float max_time){
        this.max_time = max_time;
    }

    public float Reset(){
        float tmp = this.time;
        this.time = 0;
        return tmp;
    }
    public bool Step(float delta){
        this.time += delta;
        return this.time >= this.max_time;
    }
}
