using Godot;
using System;
using System.Collections.Generic;

public class Master : Node
{
    Communication communication;
    AI agend;
    

    // this is called when the game is already set
    public void GetCommunication(AI agend, int observation, int action)
    {
        this.agend = agend;
        if (communication == null)
        {
            communication = new Communication("/tmp/cars", observation, action);
        }
        communication.Send(agend.GetObservation());
    }

    public override void _PhysicsProcess(float delta)
    {
        int action = communication.Receive();
        (float reward, List<float> observation, bool done) = agend.Step(action, delta);
        communication.Send(reward, observation, done);
        if (done) {
            GetTree().ReloadCurrentScene();
        }
    }

}
