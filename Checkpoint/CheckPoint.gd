extends Area2D

signal body_entered_area(area)
func _ready():
	self.connect("body_entered", self, "_body_entered")

func _body_entered(_sbody):
	emit_signal("body_entered_area", self)
