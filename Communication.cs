using System;
using System.Collections.Generic;
using System.IO;

public class Communication
{
    private FileStream file;

    public Communication(String path, int obs_space, int action_space)
    {
        this.file = System.IO.File.OpenWrite(path + "_client");
        this.file.WriteByte(obs_space);
        this.file.WriteByte(action_space);
    }

    public void Send(float reward, List<float> observation, bool done){

    }

    public void Send(List<float> observation){

    }

    public int Receive(){
        return 0;
    }

}
