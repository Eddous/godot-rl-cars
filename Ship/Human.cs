using Godot;
using System;

public class Human : Node
{
    public override void _PhysicsProcess(float delta)
    {
        bool[] keys = { false, false, false };
        keys[0] = Input.IsActionPressed("ui_up");
        keys[1] = Input.IsActionPressed("ui_left");
        keys[2] = Input.IsActionPressed("ui_right");
        if (((Player)GetParent()).Move(delta, keys))
        {
            GetTree().ReloadCurrentScene();
        }
    }
}
