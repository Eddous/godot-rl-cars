using Godot;
using System;
using System.Collections.Generic;

public class AI : Node
{
    private float reward;
    private List<Node2D> checkpoints;

    public void init()
    {
        ((Master)GetNode("/root/Master")).GetCommunication(this, GetObservation().Count, 4);
        GetParent().Connect("new_point", this, "NewPoint");
    }

    public (float, List<float>, bool) Step(int action, float delta)
    {
        float tmp = this.reward;
        this.reward = 0;
        return (tmp, GetObservation(), ((Player)GetParent()).Move(delta, GetKeys(action)));
    }

    private bool[] GetKeys(int action)
    {
        bool[] keys = { false, false, false };
        if (action != 3)
        {
            keys[action] = true;
        }
        return keys;
    }

    public List<float> GetObservation()
    {
        var p = ((RigidBody2D)GetParent());

        var obs = new List<float>();
        obs.Add(p.Position.x);
        obs.Add(p.Position.y);
        obs.Add(p.LinearVelocity.x);
        obs.Add(p.LinearVelocity.y);
        obs.Add(p.AngularVelocity);
        obs.Add(p.Rotation);
        foreach (Node2D ch in checkpoints)
        {
            obs.Add(ch.Position.x);
            obs.Add(ch.Position.y);
        }
        return obs;
    }

    private void NewPoint(Godot.Collections.Array checkpoints)
    {
        this.reward = 1;
        this.checkpoints = new List<Node2D>();
        foreach (object ch in checkpoints)
        {
            checkpoints.Add(ch);
        }
    }
}
