using Godot;
using System;

public class Player : RigidBody2D
{
    private const int speed = 60;
    private const int rotationSpeed = 3000;
    private const int time = 3;
    private const int type = 0;
    private Timer timer = new Timer(time);

    public override void _Ready()
    {
        GetParent().Connect("reward", this, nameof(Reward));
        switch (type)
        {
            case 0:
                this.RemoveChild(this.GetNode("AI"));
                break;

            case 1:
                this.RemoveChild(this.GetNode("Human"));
                ((AI)this.GetNode("AI")).init();
                break;
        }
    }

    public bool Move(float delta, bool[] keys)
    {
        if (keys[0]) ApplyCentralImpulse(new Vector2(1, 0).Rotated(Rotation) * speed * delta);
        if (keys[1]) ApplyTorqueImpulse(-rotationSpeed * delta);
        if (keys[2]) ApplyTorqueImpulse(rotationSpeed * delta);
        return timer.Step(delta);
    }

    private void Reward()
    {
        timer.Reset();
    }
}
